#!/bin/bash

# Accept an input argument
input=input.txt
while getopts i: flag; do
	case "${flag}" in
		i) input=${OPTARG};;
	esac
done

# Track a total and the last number
let gamma=0
let epsilon=0
let checked=0
let width=0
let lineCount=0
ones=()
#declare -a ones=( $(for i in {1..12}; do echo 0; done) )
while IFS= read -r line; do
	# Build bit counter off first line size
	if [ "${#ones}" -eq 0 ]; then
		for (( i=0; i<${#line}; i++ )); do
			ones+=(0);
		done
		let width="${#ones[@]}"
		echo "width: $width"
		echo "ones init: ${ones[*]}"
	fi

	# Count ones in bit slots
	for (( i=0; i<$width; i++ )); do
		let bit=$((${line:$i:1}))
		if [ "$bit" -eq 1 ]; then 
			(( ones[$i]++ ))
			#echo "${ones[*]}"
		fi	
	done

	let lineCount++
done < $input

# Calculate Cuttoff
let cutoff=lineCount/2
echo "lineCount: $lineCount"
echo "cutoff: $cutoff"

# Gamma = most common bits
# Epsilon = least common bits
gammaB=""
epsilonB=""
nextG=""
nextE=""
for (( i=0; i<$width; i++ )); do
	# Instructions don't specify an even split
	if [ "${ones[$i]}" -eq $cutoff ]; then echo "Error"; exit 1; fi
	
	nextG="0"
	nextE="1"
	if [ "${ones[$i]}" -gt $cutoff ]; then 
		nextG="1"
		nextE="0"
	fi

	gammaB="$gammaB$nextG"
	epsilonB="$epsilonB$nextE"
done

gamma=$((2#$gammaB))
epsilon=$((2#$epsilonB))
echo "gammaB: $gammaB ($gamma)"
echo "epsilonB: $epsilonB ($epsilon)"

let power=gamma*epsilon
echo "Power: $power"
