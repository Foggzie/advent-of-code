#!/bin/bash

# Accept an input argument
input=input.txt
while getopts i: flag; do
	case "${flag}" in
		i) input=${OPTARG};;
	esac
done

# Tracking vars
x=0
y=0
aim=0

# Read input line by line
while IFS= read -r line; do
	array=( $line )
	if [ "${array[0]}" == "forward" ]; then 
		let x+=array[1]
		let y+=aim*array[1]
	elif [ "${array[0]}" == "up" ]; then 
		let aim-=array[1]
	elif [ "${array[0]}" == "down" ]; then
		let aim+=array[1]
	fi
done < $input

let output=x*y
echo "$output"
