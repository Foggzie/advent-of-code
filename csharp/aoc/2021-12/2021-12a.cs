﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

public static class aoc2021_12a
{
    private static readonly string inputPath = System.IO.Path.Combine("2021-12", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);

        var start = default(Node);
        var end = default(Node);
        var map = new Dictionary<string, Node>();
        foreach(var line in lines) {
            var nodes = line.Split('-');
            var leftId = nodes[0];
            var rightId = nodes[1];

            if (!map.TryGetValue(leftId, out var leftNode)) {
                leftNode = new Node(leftId);
                map.Add(leftId, leftNode);
            }

            if (!map.TryGetValue(rightId, out var rightNode)) {
                rightNode = new Node(rightId);
                map.Add(rightId, rightNode);
            }

            leftNode.Connections.Add(rightNode);
            rightNode.Connections.Add(leftNode);

            if (leftId == "start") start = leftNode;
            else if (leftId == "end") end = leftNode;
            if (rightId == "start") start = rightNode;
            else if (rightId == "end") end = rightNode;
        }

        var paths = new List<Path>(new[] { new Path(start, end) });
        while (BuildPaths(paths, end)) { }

        foreach(var path in paths) {
            if (!path.Done) continue;
            Console.WriteLine($"Path: {path}");
        }

        Console.WriteLine($"\n Total Paths: {paths.Count}");
    }

    public static bool BuildPaths(
        List<Path> paths, 
        Node end)
    {
        var cachedCount = paths.Count;
        for (var i = 0; i < cachedCount; ++i) {
            var path = paths[i];
            if (path.Done) continue;
            if (path.Failed) continue;

            var appended = false;
            var oldPath = new Path(path);
            var cachedLast = path.Last;

            foreach (var nextNode in cachedLast.Connections) {
                if (!appended) {
                    path.Add(nextNode);
                    appended = true;
                }
                else {
                    var newPath = new Path(oldPath, nextNode);
                    paths.Add(newPath);
                }
            }
        }

        paths.RemoveAll(p => p.Failed);
        return paths.Any(p => !p.Done);
    }

    public class Node
    {
        public string Id { get; }
        public bool IsSmall { get; }

        public HashSet<Node> Connections = new HashSet<Node>();

        public Node(string id)
        {
            Id = id;
            if (Id != Id.ToUpper()) IsSmall = true;
        }

        public override string ToString()
        {
            return Id;
        }
    }

    public class Path
    {
        public HashSet<Node> Smalls = new HashSet<Node>();
        public List<Node> Nodes = new List<Node>();
        public Node Last => Nodes[^1];
        public bool Done => Last == End;
        public bool Failed { get; private set; }
        public Node End { get; }

        public Path(Node start, Node end)
        {
            Nodes.Add(start);
            Smalls.Add(start);
            End = end;
        }

        public Path(Path duplicate)
        {
            Nodes.AddRange(duplicate.Nodes);
            Smalls.UnionWith(duplicate.Smalls);
            End = duplicate.End;
        }

        public Path(Path duplicate, Node next)
        {
            Nodes.AddRange(duplicate.Nodes);
            Smalls.UnionWith(duplicate.Smalls);
            End = duplicate.End;
            
            Add(next);
        }

        public void Add(Node next)
        {
            Nodes.Add(next);
            if (next.IsSmall && !Smalls.Add(next)) Failed = true;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < Nodes.Count; ++i) {
                var node = Nodes[i];
                sb.Append(i == Nodes.Count - 1 ? $"{node}" : $"{node}-");
            }

            if (Failed) sb.Append(" Failed");
            if (Done) sb.Append(" Done");

            return sb.ToString();
        }
    }
}
