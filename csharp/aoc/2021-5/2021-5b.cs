﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public static class aoc2021_5b
{
    private static readonly string inputPath = Path.Combine("2021-5", "input.txt");

    public static void Run()
    {
        var overlaps = 0;
        var lines = File.ReadAllLines(inputPath);
        var map = new Dictionary<(int, int), int>();
        foreach (var line in lines) {
            var coords = line.Split("->", 
                StringSplitOptions.TrimEntries)
                .Select(coord => coord.Split(',')).ToArray();

            foreach (var coord in CoordinateRange(
                Convert.ToInt32(coords[0][0]),
                Convert.ToInt32(coords[0][1]),
                Convert.ToInt32(coords[1][0]),
                Convert.ToInt32(coords[1][1])))
            {
                map.TryGetValue(coord, out var count);
                if (count == 1) overlaps++;
                map[coord] = count + 1;
            }
        }

        Console.WriteLine(overlaps);
    }

    private static IEnumerable<(int, int)> CoordinateRange(
        int x1, int y1, int x2, int y2)
    {
        var xDiff = x2 - x1;
        var xSign = xDiff > 0 ? 1 : xDiff < 0 ? -1 : 0;
        var yDiff = y2 - y1;
        var ySign = yDiff > 0 ? 1 : yDiff < 0 ? -1 : 0;
        var count = Math.Max(Math.Abs(xDiff), Math.Abs(yDiff)) + 1;
        for (var i = 0; i < count; ++i) {
            yield return (x1 + i * xSign, y1 + i * ySign);
        }
    }
}
