﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public static class aoc2021_5a
{
    private static readonly string inputPath = Path.Combine("2021-5", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);
        var map = new Dictionary<(int, int), int>();
        var overlaps = 0;
        foreach (var line in lines) {
            var coords = line.Split("->", System.StringSplitOptions.TrimEntries)
                .Select(coord => coord.Split(',')).ToArray();
            var x1 = Convert.ToInt32(coords[0][0]);
            var y1 = Convert.ToInt32(coords[0][1]);
            var x2 = Convert.ToInt32(coords[1][0]);
            var y2 = Convert.ToInt32(coords[1][1]);
            if (x1 == x2) {
                var yMin = Math.Min(y1, y2);
                var yMax = Math.Max(y1, y2);
                for (var y = yMin; y <= yMax; ++y) {
                    var coord = (x1, y);
                    map.TryGetValue(coord, out var count);
                    if (count == 1) overlaps++;
                    map[coord] = count + 1;
                }
            }
            else if (y1 == y2) {
                var xMin = Math.Min(x1, x2);
                var xMax = Math.Max(x1, x2);
                for (var x = xMin; x <= xMax; ++x) {
                    var coord = (x, y1);
                    map.TryGetValue(coord, out var count);
                    if (count == 1) overlaps++;
                    map[coord] = count + 1;
                }
            }
        }

        Console.WriteLine(overlaps);
    }
}
