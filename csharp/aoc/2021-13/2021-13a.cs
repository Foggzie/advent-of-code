﻿using System;
using System.Collections.Generic;
using System.IO;

public static class aoc2021_13a
{
    private static readonly string inputPath = Path.Combine("2021-13", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);

        var isCoord = true;
        var coords = new List<(int, int)>();
        var folds = new List<(char, int)>();
        for (var i = 0 ; i < lines.Length; ++i) {
            var line = lines[i];
            if (String.IsNullOrWhiteSpace(line)) {
                isCoord = false;
                continue;
            }

            if (isCoord) {
                var split = line.Split(',');
                var x = Convert.ToInt32(split[0]);
                var y = Convert.ToInt32(split[1]);
                coords.Add((x, y));
            }
            else {
                var axis = line[11];
                var num = Convert.ToInt32(line[13..]);
                folds.Add((axis, num));
            }
        }

        var uniqueCoords = new HashSet<(int, int)>();
        foreach (var coord in coords) {
            var translated = Translate(coord, folds[0]);
            uniqueCoords.Add(translated);
        }

        Console.WriteLine($"{uniqueCoords.Count}");
    }

    private static (int, int) Translate((int x, int y) coord, (char axis, int num) fold)
    {
        if (fold.axis == 'x' && coord.x > fold.num) {
            var dist = coord.x - fold.num;
            var newX = coord.x - dist - dist;
            return (newX, coord.y);
        }

        else if (fold.axis == 'y' && coord.y > fold.num) {
            var dist = coord.y - fold.num;
            var newY = coord.y - dist - dist;
            return (coord.x, newY);
        }

        return (coord.x, coord.y);
    }
}

