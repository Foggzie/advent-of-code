﻿using System;
using System.Collections.Generic;
using System.IO;

public static class aoc2021_13b
{
    private static readonly string inputPath = Path.Combine("2021-13", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);

        var isCoord = true;
        var coords = new List<(int, int)>();
        var folds = new List<(char, int)>();
        for (var i = 0 ; i < lines.Length; ++i) {
            var line = lines[i];
            if (String.IsNullOrWhiteSpace(line)) {
                isCoord = false;
                continue;
            }

            if (isCoord) {
                var split = line.Split(',');
                var x = Convert.ToInt32(split[0]);
                var y = Convert.ToInt32(split[1]);
                coords.Add((x, y));
            }
            else {
                var axis = line[11];
                var num = Convert.ToInt32(line[13..]);
                folds.Add((axis, num));
            }
        }

        var uniqueCoords = new HashSet<(int, int)>();
        var maxX = 0;
        var maxY = 0;
        foreach (var coord in coords) {
            var translated = coord;
            foreach (var fold in folds) {
                translated = Translate(translated, fold);
            }
            uniqueCoords.Add(translated);
            if (translated.Item1 > maxX) maxX = translated.Item1;
            if (translated.Item2 > maxY) maxY = translated.Item2;
        }

        for (var y = 0; y <= maxY; y++) {
            for (var x = 0; x <= maxX; x++) {
                Console.Write(uniqueCoords.Contains((x, y)) ? "#" : " ");
            }
            Console.WriteLine();
        }
    }

    private static (int, int) Translate(
        (int x, int y) coord, 
        (char axis, int num) fold)
    {
        if (fold.axis == 'x' && coord.x > fold.num) {
            var dist = coord.x - fold.num;
            var newX = coord.x - dist - dist;
            return (newX, coord.y);
        }

        else if (fold.axis == 'y' && coord.y > fold.num) {
            var dist = coord.y - fold.num;
            var newY = coord.y - dist - dist;
            return (coord.x, newY);
        }

        return (coord.x, coord.y);
    }
}

