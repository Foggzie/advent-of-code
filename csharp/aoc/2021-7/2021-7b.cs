﻿using System;
using System.IO;
using System.Linq;

public static class aoc2021_7b
{
    private const string example = "16,1,2,0,4,2,7,1,2,14";
    private static readonly string inputPath = Path.Combine("2021-7", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);
        var input = lines[0];

        var nums = input.Split(',')
            .Select(s => Convert.ToInt32(s))
            .ToList();
        nums.Sort();

        var sum = 0;
        foreach (var num in nums) { sum += num; }

        var positionA = sum / nums.Count;
        var positionB = sum / nums.Count + 1;

        var costA = 0;
        var costB = 0;

        for (var i = 0; i < nums.Count; ++i) {
            var diffA = Math.Abs(positionA - nums[i]);
            var diffB = Math.Abs(positionB - nums[i]);
            costA += (diffA * (diffA + 1)) / 2;
            costB += (diffB * (diffB + 1)) / 2;
        }

        var fuelCost = Math.Min(costA, costB);
        Console.WriteLine($"{fuelCost}");
    }
}
