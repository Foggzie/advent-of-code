﻿using System;
using System.IO;
using System.Linq;

public static class aoc2021_7a
{
    private const string example = "16,1,2,0,4,2,7,1,2,14";
    private static readonly string inputPath = Path.Combine("2021-7", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);
        var input = lines[0];

        var nums = input.Split(',')
            .Select(s => Convert.ToInt32(s))
            .ToList();
        nums.Sort();

        var index = nums.Count / 2;
        var best = nums[index];
        var fuelCost = 0;
        for (var i = 0; i < nums.Count; ++i) {
            fuelCost += Math.Abs(best - nums[i]);
        }

        Console.WriteLine($"{fuelCost}");
    }
}
