﻿using System;
using System.Collections.Generic;
using System.IO;

public static class aoc2021_14ab
{
    private static readonly string inputPath = Path.Combine("2021-14", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);
        var polymerTemplate = lines[0];

        var charScores = new Dictionary<char, long>();
        var map = new Dictionary<(char, char), char>();
        var keyScores = new Dictionary<(char, char), long>();
        foreach (var line in lines[2..]) {
            var split = line.Split(" -> ");
            var key = split[0];
            var value = split[1][0];
            map.Add((key[0], key[1]), value);

            keyScores[(key[0], key[1])] = 0;
            charScores[key[0]] = 0;
            charScores[key[1]] = 0;
            charScores[value] = 0;
        }

        for (var i = 0; i < polymerTemplate.Length - 1; ++i) {
            keyScores[(polymerTemplate[i], polymerTemplate[i + 1])]++;
            charScores[polymerTemplate[i]]++;
        }
        charScores[polymerTemplate[^1]]++;

        var keyScoreDiffs = new Dictionary<(char, char), long>();
        for (var i = 0; i < 40; ++i) {
            keyScoreDiffs.Clear();

            foreach (var kvp in map) {
                if (keyScores.TryGetValue(kvp.Key, out var keyScore)) {
                    keyScoreDiffs.TryGetValue(kvp.Key, out var lostKeyScore);
                    keyScoreDiffs[kvp.Key] = lostKeyScore - keyScore;
                    charScores[kvp.Value] += keyScore;

                    var leftKey = (kvp.Key.Item1, kvp.Value);
                    keyScoreDiffs.TryGetValue(leftKey, out var leftKeyScore);
                    keyScoreDiffs[leftKey] = leftKeyScore + keyScore;

                    var rightKey = (kvp.Value, kvp.Key.Item2);
                    keyScoreDiffs.TryGetValue(rightKey, out var rightKeyScore);
                    keyScoreDiffs[rightKey] = rightKeyScore + keyScore;
                }
            }

            foreach (var kvp in keyScoreDiffs) {
                keyScores.TryGetValue(kvp.Key, out var oldScore);
                keyScores[kvp.Key] = oldScore + kvp.Value;
            }
        }

        var max = long.MinValue;
        var min = long.MaxValue;
        foreach (var score in charScores.Values) {
            if (score > max) max = score;
            if (score < min) min = score;
        }

        Console.WriteLine($"{max}-{min} = {max-min}");
    }
}
