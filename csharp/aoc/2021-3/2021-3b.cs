﻿using System;
using System.IO;

public static class aoc2021_3b
{
    private const int bitWidth = 12;
    private static readonly string inputPath = Path.Combine("2021-3", "input.txt");

    public static void Run()
    {
        var tree = new Node();
        var lineCount = 0;
        var ones = new int[bitWidth];
        foreach (var line in File.ReadLines(inputPath)) {
            lineCount++;
            for (var i = 0; i < bitWidth; i++) {
                ones[i] += line[i] == '1' ? 1 : 0;
            }
            tree.Insert(line);
        }

        var oxygenBinary = tree.Find(1, true);
        var co2Binary = tree.Find(0, false);

        var oxygen = Convert.ToInt32(oxygenBinary, 2);
        var co2 = Convert.ToInt32(co2Binary, 2);
        var lifeSupport = oxygen * co2;
        Console.WriteLine($"Oxygen: {oxygenBinary} ({oxygen})");
        Console.WriteLine($"CO2: {co2Binary} ({co2})");
        Console.WriteLine($"Life Support: {lifeSupport}");
    }

    private class Node
    {
        public Node[] child = new Node[2];

        public int Size { get; private set; }

        public bool IsLeaf => child[0] == default && child[1] == default;
        public bool Single => child[0] == default ^ child[1] == default;
        public int Only => !Single ? throw new InvalidOperationException() : child[0] == default ? 1 : 0;

        public void Insert(string value)
        {
            Size++;
            var i = value[0] == '1' ? 1 : 0;
            child[i] ??= new Node();

            if (value.Length > 1) {
                child[i].Insert(value[1..]);
            }
        }

        public string Find(int equalityPerference, bool mostUsed, string output = "")
        {
            if (IsLeaf) {
                return output;
            }

            int next;
            if (Single) {
                next = Only;
            }
            else {
                var size0 = child[0].Size;
                var size1 = child[1].Size;
                if (size0 == size1) next = equalityPerference;
                else if (mostUsed) next = size0 > size1 ? 0 : 1;
                else next = size0 < size1 ? 0 : 1;
            }

            output += $"{next}";
            return child[next].Find(equalityPerference, mostUsed, output);
        }
    }
}
