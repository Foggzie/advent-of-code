﻿using System;
using System.Collections.Generic;
using System.IO;

public static class aoc2021_11b
{
    private static readonly string inputPath = Path.Combine("2021-11", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);

        var width = lines[0].Length;
        var height = lines.Length;
        var map = new int[width, height];

        for (var x = 0; x < width; ++x) {
            for (var y = 0; y < height; ++y) {
                map[x, y] = Convert.ToInt32(lines[y][x].ToString());
            }
        }

        for (var step = 1; step <= 1000; ++step) {
            var flashed = new HashSet<(int, int)>();
            for (var x = 0; x < width; ++x) {
                for (var y = 0; y < height; ++y) {
                    StepPoint(width, height, map, (x, y), flashed);
                }
            }

            if (flashed.Count == map.Length) {
                Console.WriteLine($"Sync Flash on Step {step}");
                break;
            }
            foreach (var flash in flashed) map[flash.Item1, flash.Item2] = 0;
        }
    }

    private static void StepPoint(
        int width,
        int height,
        int[,] map,
        (int x, int y) point,
        HashSet<(int, int)> flashed)
    {
        if (flashed.Contains(point)) return;
        if (++map[point.x, point.y] <= 9) return;
        flashed.Add(point);

        if (point.x > 0) {
            var nextX = point.x - 1;
            StepPoint(width, height, map, (nextX, point.y), flashed);
            if (point.y > 0) StepPoint(width, height, map, (nextX, point.y - 1), flashed);
            if (point.y < height - 1) StepPoint(width, height, map, (nextX, point.y + 1), flashed);
        }

        if (point.x < width - 1) {
            var nextX = point.x + 1;
            StepPoint(width, height, map, (nextX, point.y), flashed);
            if (point.y > 0) StepPoint(width, height, map, (nextX, point.y - 1), flashed);
            if (point.y < height - 1) StepPoint(width, height, map, (nextX, point.y + 1), flashed);
        }

        if (point.y > 0) StepPoint(width, height, map, (point.x, point.y - 1), flashed);
        if (point.y < height - 1) StepPoint(width, height, map, (point.x, point.y + 1), flashed);
    }
}
