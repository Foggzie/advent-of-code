﻿using System;
using System.IO;

public static class aoc2021_6a
{
    private const int newAge = 9;
    private const int resetAge = 7;
    private const int days = 256;
    private static readonly string inputPath = Path.Combine("2021-6", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);
        var nums = new ulong[newAge];
        var scratch = new ulong[newAge];

        foreach (var c in lines[0]) {
            if (c == ',') continue;
            var next = Convert.ToInt32(c.ToString());
            nums[next]++;
        }

        Console.WriteLine("Initial State: ");
        for (var i = 0; i < nums.Length; ++i) {
            Console.WriteLine($"[{i}]= {nums[i]}");
        }

        for (var i = 1; i <= days; ++i) {
            Step(nums, scratch);
        }

        var total = 0UL;
        Console.WriteLine($"\nEnding State: ");
        for (var i = 0; i < nums.Length; ++i) {
            total += nums[i];
            Console.WriteLine($"[{i}]= {nums[i]}");
        }

        Console.WriteLine($"\nTotal: {total}");
    }

    private static void Step(ulong[] nums, ulong[] scratch)
    {
        for (var i = newAge - 1; i >= 0; --i) {
            if (i == 0) {
                scratch[resetAge - 1] += nums[i];
                scratch[newAge - 1] = nums[i];
            }
            else {
                scratch[i - 1] = nums[i];
            }
        }

        for (var i = 0; i < newAge; ++i) {
            nums[i] = scratch[i];
        }
    }
}
