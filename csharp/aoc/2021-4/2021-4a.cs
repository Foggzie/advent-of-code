﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

public static class aoc2021_4a
{
    private const int boardSize = 5;
    private static readonly string inputPath = Path.Combine("2021-4", "input.txt");

    public static async Task Run()
    {
        // Read File into array
        var readFileTask = File.ReadAllLinesAsync(inputPath);
        await readFileTask;
        var lines = readFileTask.Result;

        // Retrieve number sequence
        var sequence = lines[0].Split(',', 
            StringSplitOptions.RemoveEmptyEntries | 
            StringSplitOptions.TrimEntries)
            .Select(s => Convert.ToInt32(s));

        // Prep our first board
        var boards = new List<Board>();
        boards.Add(Board.New());
        var currentBoard = boards[^1];

        // Buid each board
        for (var i = 1; i < lines.Length; ++i) {
            if (string.IsNullOrWhiteSpace(lines[i])) {
                continue;
            }

            // Start a new board if needed
            if (currentBoard.Ready) {
                boards.Add(Board.New());
                currentBoard = boards[^1];
            }

            // Run through the whole line
            var numbers = lines[i].Split(' ',
                StringSplitOptions.RemoveEmptyEntries |
                StringSplitOptions.TrimEntries)
                .Select(s => Convert.ToInt32(s));
            foreach(var number in numbers) {
                currentBoard.Add(number);
            }
        }

        Console.WriteLine($"{boards.Count} Boards");

        // "Call out" numbers until a board wins
        var winner = false;
        foreach(var number in sequence) {
            foreach(var board in boards) {
                if (board.Won) {
                    continue;
                }
                if (board.Check(number)) {
                    Console.WriteLine($"Winner on #{number} with score {board.Score}");
                    winner = true;
                }
            }

            if (winner) break;
        }
    }

    private class V2i
    {
        public int X;
        public int Y;
        public bool Hit;
    }

    private class Board
    {
        private readonly Dictionary<int, V2i> map = new();
        private readonly bool[,] hits;
        private readonly int size;

        public bool Ready => map.Count == size * size;
        public bool Won { get; private set; }
        public int Score { get; private set; }
        public int WinningNumber { get; private set; }

        public Board(int size) 
        {
            this.size = size;
            hits = new bool[size, size];
        }

        public void Add(int input) {
            var x = map.Count % size;
            var y = map.Count / size;
            map.Add(input, new V2i { X = x, Y = y });
        }

        public bool Check(int number)
        {
            if (Won) return true;

            if (map.TryGetValue(number, out var v2i)) {
                hits[v2i.X, v2i.Y] = true;
                v2i.Hit = true;

                if (Evaluate()) {
                    Won = true;
                    v2i.Hit = true;
                    WinningNumber = number;
                    SetScore();
                    return true;
                }
            }

            return false;
        }

        private void SetScore()
        {
            Score = 0;
            foreach (var (number, spot) in map) {
                if (!spot.Hit) {
                    Score += number;
                }
            }

            Score *= WinningNumber;
        }

        private bool Evaluate()
        {
            var y = 0;
            var x = 0;
            var won = true;

            // Top
            for (y = 0, x = 0; x < size; x++) {
                if (!hits[x, y]) break;
                if (x == size - 1) return true;
            }

            // Bottom
            for (y = size - 1, x = 0; x < size; x++) {
                if (!hits[x, y]) break;
                if (x == size - 1) return true;
            }

            // Left
            for (x = 0, y = 0; y < size; y++) {
                if (!hits[x, y]) break;
                if (y == size - 1) return true;
            }

            // Right
            for (x = size - 1, y = 0; y < size; y++) {
                if (!hits[x, y]) break;
                if (y == size - 1) return true;
            }

            // Diagonals UL-LR
            for (x = 0, y = 0; x < size; ++x, ++y) {
                if (!hits[x, y]) break;
                if (x == size - 1) return true;
            }

            // Diagonals LL-UR
            for (x = 0, y = size - 1; x < size; ++x, --y) {
                if (!hits[x, y]) break;
                if (x == size - 1) return true;
            }

            return false;
        }

        public static Board New()
        {
            return new Board(boardSize);
        }
    }
}
