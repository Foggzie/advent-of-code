﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

public static class aoc2021_4b
{
    private const int boardSize = 5;
    private static readonly string inputPath = Path.Combine("2021-4", "input.txt");

    public static void Run()
    {
        // Read File into array
        var lines = File.ReadAllLines(inputPath);

        // Retrieve number sequence
        var sequence = lines[0].Split(',',
            StringSplitOptions.RemoveEmptyEntries |
            StringSplitOptions.TrimEntries)
            .Select(s => Convert.ToInt32(s));

        // Prep our first board
        var boards = new List<Board>();
        boards.Add(NewBoard());
        var currentBoard = boards[^1];

        // Buid each board
        for (var i = 1; i < lines.Length; ++i) {
            if (string.IsNullOrWhiteSpace(lines[i])) {
                continue;
            }

            // Start a new board if needed
            if (currentBoard.Ready) {
                //PrintBoard(currentBoard);
                //Console.WriteLine();

                boards.Add(NewBoard());
                currentBoard = boards[^1];
            }

            // Run through the whole line
            var numbers = lines[i].Split(' ',
                StringSplitOptions.RemoveEmptyEntries |
                StringSplitOptions.TrimEntries)
                .Select(s => Convert.ToInt32(s));
            foreach (var number in numbers) {
                currentBoard.Add(number);
            }
        }

        Console.WriteLine($"{boards.Count} Boards");

        // "Call out" numbers
        foreach (var number in sequence) {
            foreach (var board in boards) {
                if (!board.Won && board.Check(number)) {
                    Console.WriteLine($"Winner on #{board.WinningNumber} score {board.Score}");
                    PrintBoard(board);
                }
            }
        }
    }
    private static Board NewBoard()
    {
        return new Board(boardSize);
    }

    private static void PrintBoard(Board board)
    {
        for (var i = 0; i < board.Layout.Length; ++i) {
            var x = i % board.Size;
            var sb = new StringBuilder();
            var num = board.Layout[i];
            if (num < 10) sb.Append(' ');
            if (num < 100) sb.Append(' ');
            sb.Append(num);
            sb.Append(' ');
            var beforeColor = Console.ForegroundColor;
            if (board.Map[num].Hit) Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(sb);
            if (x == board.Size - 1) Console.WriteLine();
            Console.ForegroundColor = beforeColor;
        }
    }

    private class V2i
    {
        public int X;
        public int Y;
        public bool Hit;
    }

    private class Board
    {
        private readonly Dictionary<int, V2i> map = new();
        private readonly bool[,] hits;
        private readonly int[] layout;
        private readonly int size;

        public IReadOnlyDictionary<int, V2i> Map => map;
        public bool Ready => map.Count == size * size;
        public bool Won { get; private set; }
        public int Size => size;
        public int Score { get; private set; }
        public int WinningNumber { get; private set; }
        public int[] Layout => layout;

        public Board(int size)
        {
            this.size = size;
            hits = new bool[size, size];
            layout = new int[size * size];
        }

        public void Add(int nextNumber)
        {
            layout[map.Count] = nextNumber;
            var x = map.Count % size;
            var y = map.Count / size;
            map.Add(nextNumber, new V2i { X = x, Y = y });
        }
        
        public bool Check(int number)
        {
            if (Won) return true;

            if (map.TryGetValue(number, out var v2i)) {
                hits[v2i.X, v2i.Y] = true;
                v2i.Hit = true;

                if (HasWinningBoard()) {
                    Won = true;
                    SetScore(number);
                    return true;
                }
            }

            return false;
        }

        private void SetScore(int winningNumber)
        {
            WinningNumber = winningNumber;
            Score = 0;
            foreach (var (number, spot) in map) {
                if (!spot.Hit) {
                    Score += number;
                }
            }

            Score *= WinningNumber;
        }

        private bool HasWinningBoard()
        {
            var y = 0;
            var x = 0;

            // Columns
            for (x = 0; x < size; ++x) {
                for (y = 0; y < size; ++y) {
                    if (!hits[x, y]) break;
                    if (y == size - 1) return true;
                }
            }

            // Rows
            for (y = 0; y < size; ++y) {
                for (x = 0; x < size; ++x) {
                    if (!hits[x, y]) break;
                    if (x == size - 1) return true;
                }
            }

            return false;
        }
    }
}
