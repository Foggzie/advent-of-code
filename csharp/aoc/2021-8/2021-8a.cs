﻿using System;
using System.Collections.Generic;
using System.IO;

public static class aoc2021_8a
{
    private static readonly StringSplitOptions pure =
        StringSplitOptions.RemoveEmptyEntries |
        StringSplitOptions.TrimEntries;

    private static readonly string inputPath = Path.Combine("2021-8", "exampleInput.txt");// input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);
        
        var total = 0;
        foreach (var line in lines) {
            var piped = line.Split('|', pure);
            var inputs = piped[1].Split(' ', pure);
            foreach (var rightValue in inputs) {
                if (rightValue.Length == 2 || 
                    rightValue.Length == 3 || 
                    rightValue.Length == 4 || 
                    rightValue.Length == 7) {
                    total++;
                }
            }
        }

        Console.WriteLine($"Total: {total}");
    }
}

