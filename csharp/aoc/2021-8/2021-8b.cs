﻿using System;
using System.Collections.Generic;
using System.IO;

public static class aoc2021_8b
{
    private static readonly StringSplitOptions pure =
        StringSplitOptions.RemoveEmptyEntries |
        StringSplitOptions.TrimEntries;

    private static readonly string inputPath = Path.Combine("2021-8", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);
        var total = 0;

        var digits = new Dictionary<int, int>()
        {
            { 42, 0 }, { 17, 1 }, { 34, 2 }, { 39, 3 }, { 30, 4 },
            { 37, 5 }, { 41, 6 }, { 25, 7 }, { 49, 8 }, { 45, 9 },
        };

        foreach (var line in lines) {
            var piped = line.Split('|', pure);

            var charCounts = new Dictionary<char, int>();
            foreach (var c in piped[0]) {
                if (c < 'a' || c > 'g') continue;
                charCounts.TryGetValue(c, out var count);
                charCounts[c] = ++count;
            }

            var number = 0;
            foreach (var outputDigit in piped[1].Split(' ', pure)) {
                var digit = 0;
                foreach (var c in outputDigit) { digit += charCounts[c]; }

                number *= 10;
                number += digits[digit];
            }

            total += number;
        }

        Console.WriteLine($"Total: {total}");
    }
}

