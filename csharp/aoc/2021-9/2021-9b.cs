﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public static class aoc2021_9b
{
    private static readonly string inputPath = Path.Combine("2021-9", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);

        var width = lines[0].Length;
        var height = lines.Length;
        var map = new int[width, height];

        for (var y = 0; y < lines.Length; ++y) {
            var line = lines[y];
            for (var x = 0; x < line.Length; ++x) {
                map[x, y] = Convert.ToInt32(line[x].ToString());
            }
        }

        var basins = new List<HashSet<(int, int)>>();
        var observed = new HashSet<(int, int)>();
        
        for (var y = 0; y < height; ++y) {
            for (var x = 0; x < width; ++x) {
                var point = (x, y);
                if (observed.Contains(point)) continue;
                if (map[x, y] == 9) continue;

                var basin = new HashSet<(int, int)>();
                basins.Add(basin);
                BuildBasin(width, height, map, (x, y), basin, observed);
            }
        }

        var largest = basins.ToList();
        largest.Sort((l, r) => r.Count.CompareTo(l.Count));

        var answer = 1;
        for (var i = 0; i < 3 && i < largest.Count; ++i) answer *= largest[i].Count;
        Console.WriteLine($"Answer: {answer}");
    }

    private static void BuildBasin(
        int w, // width
        int h, // height
        int [,] m, // map
        (int x, int y) p, // point
        HashSet<(int, int)> b, // basin
        HashSet<(int, int)> o) // observed
    {
        if (o.Contains(p)) return;
        if (m[p.x, p.y] == 9) return;

        b.Add(p);
        o.Add(p);

        if (p.x != 0) BuildBasin(w, h, m, (p.x - 1, p.y), b, o);
        if (p.x < w - 1) BuildBasin(w, h, m, (p.x + 1, p.y), b, o);
        if (p.y != 0) BuildBasin(w, h, m, (p.x, p.y - 1), b, o);
        if (p.y < h - 1) BuildBasin(w, h, m, (p.x, p.y + 1), b, o);
    }
}
