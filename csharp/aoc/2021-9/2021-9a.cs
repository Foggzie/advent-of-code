﻿using System;
using System.IO;

public static class aoc2021_9a
{
    private static readonly string inputPath = Path.Combine("2021-9", "input.txt");

    public static void Run()
    {
        var lines = File.ReadAllLines(inputPath);

        var width = lines[0].Length;
        var height = lines.Length;
        var map = new int[width, height];

        for (var y = 0; y < lines.Length; ++y) {
            var line = lines[y];
            for (var x = 0; x < line.Length; ++x) {
                map[x, y] = Convert.ToInt32(line[x].ToString());
            }
        }

        var totalRisk = 0;
        for (var y = 0; y < height; ++y) {
            for (var x = 0; x < width; ++x) {
                var h = map[x, y];
                if (x != 0 && h >= map[x - 1, y]) continue;
                if (x != width - 1 && h >= map[x + 1, y]) continue;
                if (y != 0 && h >= map[x, y - 1]) continue;
                if (y != height - 1 && h >= map[x, y + 1]) continue;
                Console.WriteLine($"LowPoint[{x},{y}] = {h}");
                totalRisk += h + 1;
            }
        }

        Console.WriteLine($"Risk: {totalRisk}");
    }
}
