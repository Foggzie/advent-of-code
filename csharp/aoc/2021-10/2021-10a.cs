﻿using System;
using System.Collections.Generic;
using System.IO;

public static class aoc2021_10a
{
    private static readonly string inputPath = Path.Combine("2021-10", "input.txt");

    public static void Run()
    {
        var values = new Dictionary<char, ulong>() {
            { ')', 3 }, { ']', 57 }, { '}', 1197 }, { '>', 25137 },
        };
        var openToClose = new Dictionary<char, char>() {
            { '(', ')' }, { '[', ']' }, { '{', '}' }, { '<', '>' },
        };


        var lines = File.ReadAllLines(inputPath);

        var score = 0UL;
        foreach (var line in lines) {
            var expectations = new Stack<char>();
            foreach (var c in line) {
                if (expectations.Count == 0 || openToClose.ContainsKey(c)) {
                    expectations.Push(openToClose[c]);
                    continue;
                }

                var expected = expectations.Peek();
                if (c == expected) {
                    expectations.Pop();
                }
                else {
                    score += values[c];
                    break;
                }
            }
        }

        Console.WriteLine($"Score: {score}");
    }
}
