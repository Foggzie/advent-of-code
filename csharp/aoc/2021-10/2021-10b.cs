﻿using System;
using System.Collections.Generic;
using System.IO;

public static class aoc2021_10b
{
    private static readonly string inputPath = Path.Combine("2021-10", "input.txt");

    public static void Run()
    {
        var values = new Dictionary<char, ulong>() {
            { ')', 1 }, { ']', 2 }, { '}', 3 }, { '>', 4 },
        };
        var openToClose = new Dictionary<char, char>() {
            { '(', ')' }, { '[', ']' }, { '{', '}' }, { '<', '>' },
        };


        var lines = File.ReadAllLines(inputPath);
        
        var scores = new List<ulong>();
        foreach (var line in lines) {
            var expectations = new Stack<char>();
            var corrupt = false;
            foreach (var c in line) {
                if (expectations.Count == 0 || openToClose.ContainsKey(c)) {
                    expectations.Push(openToClose[c]);
                    continue;
                }

                var expected = expectations.Peek();
                if (c == expected) {
                    expectations.Pop();
                }
                else {
                    corrupt = true;
                    break;
                }
            }

            if (corrupt) continue;

            var score = 0UL;
            while (expectations.Count > 0) {
                score *= 5;
                score += values[expectations.Pop()];
            }

            scores.Add(score);
            Console.WriteLine($"Score: {score}");
        }

        scores.Sort();
        Console.WriteLine($"Winner: {scores[scores.Count / 2]}");
    }
}
