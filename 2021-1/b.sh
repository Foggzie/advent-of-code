#!/bin/bash

# Accept an input argument
input=input.txt
width=3
while getopts "i:w:" flag; do
	case "${flag}" in
		i) input=${OPTARG};;
		w) width=${OPTARG};;
	esac
done

# Stuff to track
unset sumLast
total=0
n=0
let tail=width-1

values=()
for (( i=0; i<=$tail; i++ )); do values+=(-1); done

# Read input line by line
while IFS= read -r line; do
	values[$((n%width))]=$((line))
	let n++
	sum=0
	hasSum=true

	for value in ${values[@]}; do
		if [ "$value" -lt 0 ]; then hasSum=false; break; fi
		let sum+=value
	done

	if $hasSum; then
		if [ ! -z "$sumLast" ] && [ "$sumLast" -lt "$sum" ]; then
			let total++
		fi
		let sumLast=sum
	fi
done < $input

echo "$total"
