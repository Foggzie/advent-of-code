#!/bin/bash

# Accept an input argument
input=input.txt
while getopts i: flag; do
	case "${flag}" in
		i) input=${OPTARG};;
	esac
done

# Track a total and the last number
total=0
unset previous

# Read input line by line
while IFS= read -r line; do
	if [ -z "$previous" ]; then
		previous=$line
		continue
	else
		if [ "$previous" -lt "$line" ]; then
			((total++))
		fi
		previous=$line
	fi
done < $input

echo "$total"
